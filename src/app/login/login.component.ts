import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { take } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],

})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  // subscriptions: Subscription[] = [];
  constructor(private authService: AuthenticationService, private fb: FormBuilder, private router: Router) { }


  ngOnInit() {
    this.loginForm = this.fb.group({
      mail: ['', [Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'), Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8)]],
    });

  }

  // ngOnDestroy(): void {
  //   this.subscriptions.forEach(sub => {
  //     sub.unsubscribe();
  //   });
  // }

  onSubmit() {
    if(!this.loginForm.valid)
      return;
    this.authService
      .login({
        ...this.loginForm.value,
      })
      .pipe(take(1))
      .subscribe();
  }

}
