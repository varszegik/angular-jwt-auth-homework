import { Component, OnInit, PipeTransform } from '@angular/core';
import { Observable, merge } from 'rxjs';
import { FormControl } from '@angular/forms';
import { DecimalPipe } from '@angular/common';
import { startWith, map } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

interface Country {
  name: string;
  flag: string;
  area: number;
  population: number;
}

const COUNTRIES: Country[] = [
  {
    name: 'Russia',
    flag: 'f/f3/Flag_of_Russia.svg',
    area: 17075200,
    population: 146989754
  },
  {
    name: 'Canada',
    flag: 'c/cf/Flag_of_Canada.svg',
    area: 9976140,
    population: 36624199
  },
  {
    name: 'United States',
    flag: 'a/a4/Flag_of_the_United_States.svg',
    area: 9629091,
    population: 324459463
  },
  {
    name: 'China',
    flag: 'f/fa/Flag_of_the_People%27s_Republic_of_China.svg',
    area: 9596960,
    population: 1409517397
  }
];

function search(text: string, pipe: PipeTransform): Country[] {
  return COUNTRIES.filter(country => {
    const term = text.toLowerCase();
    return country.name.toLowerCase().includes(term)
        || pipe.transform(country.area).includes(term)
        || pipe.transform(country.population).includes(term);
  });
}


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [DecimalPipe]
})
export class HomeComponent implements OnInit {
  images = [1200, 801, 802].map((n) => `http://placekitten.com/g/${n}/300`);
  countries$: Observable<Country[]>;
  filter = new FormControl('');

  page = 1;
  pageSize = 2;
  collectionSize = COUNTRIES.length;

  closeResult = '';

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'gallery', size: 'lg'}).result;
  }


  constructor(private pipe: DecimalPipe, private modalService: NgbModal) {}

  onPageChange(){
    this.countries$ = this.filter.valueChanges.pipe(
      startWith(''),
      map(text => search(text, this.pipe).slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize))
    );
  }

  ngOnInit() {
    this.onPageChange();
  }

}
