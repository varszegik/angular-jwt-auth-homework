import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppLogin } from '../interfaces/interfaces';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private loggedIn = new BehaviorSubject<boolean>(false);
  constructor(private httpClient: HttpClient, private router: Router) { }

  login(body: AppLogin) {
    return this.httpClient.post<{token:  string}>(environment.apiUrl + '/login', body).pipe(tap(res => {
      localStorage.setItem('access_token', res.token);
      this.loggedIn.next(true);
    }))
  }

  logout() {
    localStorage.removeItem('access_token');
    this.loggedIn.next(false);
    this.router.navigate(['login']);
  }

  get isProfileAvailable(){
    return this.loggedIn.asObservable();
  }

  isLoggedIn(){
    return this.loggedIn.value;
  }

  getToken(){
    return localStorage.getItem('access_token');
  }
}
