import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor(private http: HttpClient) {}

  getStatus() {
    return this.http.get<{status:string}>(environment.apiUrl + '/status/authenticated');
  }

}
