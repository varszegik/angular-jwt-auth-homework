import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'angular-jwt-auth-homework';
  loggedin$: Observable<boolean>;
  constructor(private router: Router, private auth: AuthenticationService) {}

  ngOnInit(){
    this.loggedin$ = this.auth.isProfileAvailable;
  }
}
